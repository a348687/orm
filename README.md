### Mi primera API con express + ORM.

Web Plataforms.

### Unidad II Programación web de parte del servidor.

### Base de datos a utilizar:

SQL

### Autor.

**Jocelyn Soto Avila**

**Matrícula: 348687**

### Docente.

Ing. Luis Antonio Ramírez Martínez

### Descripción del ejercicio.

`Vamos a construir todos los servicios web relacionados con el ejemplo del video club, basado en el modelo entidad - relación cree para cada entidad todo su CRUD. Una vez terminado suba su proyecto a gitlab en una rama diferente a master. Suba la liga del proyecto una vez terminado.`
